from random import randint

def guess():
  name = input("Hi! What is your name?")

  for num in range(5):

    month = randint(1, 12)
    year = randint(1924, 2004)

    print('Guess', num+1, name + ' were you born in', month, "/", year, '? yes or no')

    answer = input()

    if answer == "yes":
      print("I knew it!")
      return True

    elif answer == 4:
      print("Drat! Let me try again!")


  print("I have other things to do. Good bye.")
  return False

guess()
