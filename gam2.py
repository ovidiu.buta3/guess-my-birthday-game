from random import randint

name = input("Hi! What is your name?")

answer = None

for num in range(5):

  month = randint(1, 12)
  year = randint(1924, 2004)

  print('Guess', num+1, name + ' were you born in', month, "/", year, '? yes or no')

  answer = input()

  if answer == "yes":
    print("I knew it!")
    break

  elif answer == 4:
    print("Drat! Let me try again!")

if answer == "no":
  print("I have other things to do. Good bye.")
